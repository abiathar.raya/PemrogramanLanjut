package Tugas2;

import java.util.ArrayList;
import java.util.Scanner;

public class Mahasiswa {
    ArrayList<String> namas = new ArrayList<>();
    ArrayList<String> nims = new ArrayList<>();
    ArrayList<String> alamats = new ArrayList<>();
    String nim;
    String nama;
    String alamat;

        void setAtribut() {
            Scanner input = new Scanner(System.in);
            boolean next = true;
            while (next) {
                System.out.print("Masukkan nim : ");
                this.nim = input.nextLine();
                nims.add(this.nim);

                System.out.print("Masukkan nama : ");
                this.nama = input.nextLine();
                namas.add(this.nama);

                System.out.print("Masukkan alamat: ");
                this.alamat = input.nextLine();
                alamats.add(this.alamat);

                System.out.print("Tambah lagi? (Ketik \"y\" jika iya, Ketik \"t\" jika tidak): ");
                String tambah = input.nextLine();

                if (tambah.equalsIgnoreCase("t")) {
                    next = false;
                }
        }

        input.close();
        }

        void displayInfo() {
            System.out.println("============================================");
            for (int i = 0; i < nims.size(); i++) {
                System.out.println(nims.get(i) + " | " + namas.get(i) + " | " + alamats.get(i));
            }
            System.out.println("============================================");
        }
}
